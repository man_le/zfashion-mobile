import React from 'react';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import rootEpic from './src/epic/epics';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import * as reducer from './src/reducer';

import Layout from './src/component/layout/Layout';

// const createStoreWithMiddleware = 
//       compose(
//             applyMiddleware(thunk)(createStore),
//             applyMiddleware(createEpicMiddleware(rootEpic))
//         )
  
const reducers = combineReducers(reducer);
// const store =   createStoreWithMiddleware(reducers);

const store = createStore(
        reducers,
        {},
        compose(
            applyMiddleware(thunk),
            applyMiddleware(createEpicMiddleware(rootEpic)),
        )
    );


export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Layout/>
      </Provider>
    );
  }
}

