import * as types from './../../action/types';
import * as obj from './../../action/products/productListActions';

export default function productListReducer(state=obj.defaultState(), action = {}) {
  switch (action.type) {

    case types.PRODUCT_LIST:
      console.log('==========');
      console.log(action);
      return action;

    default:
      return state;
  }
}
