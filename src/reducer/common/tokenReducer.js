import * as types from './../../action/types';
import * as obj from './../../action/common/tokenActions';

export default function tokenReducer(state=obj.defaultState(), action = {}) {
  switch (action.type) {

    case types.KEEP_TOKEN:
      return action;

    default:
      return state;
  }
}
