import * as types from './../../action/types';
import * as obj from './../../action/common/errorActions';

export default function errorReducer(state=obj.defaultState(), action = {}) {
  switch (action.type) {

    case types.ERROR:
    case types.RESET_ERROR:
      return action;

    default:
      return state;
  }
}
