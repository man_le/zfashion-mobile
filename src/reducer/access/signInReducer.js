import * as types from './../../action/types';
import * as obj from './../../action/access/signInActions';

export default function signInReducer(state=obj.defaultState(), action = {}) {
  switch (action.type) {

    case types.SIGN_IN:
    case types.SIGN_IN_SUCCESS:
      return action;

    case types.ERROR:
      let stateClone = Object.assign({}, state);
      stateClone.loading = false;
      return stateClone;

    default:
      return state;
  }
}
