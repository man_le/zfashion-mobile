import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import {AppRegistry, AsyncStorage} from 'react-native';
import { StyleProvider, Container, Content, Tab, Tabs, Header, TabHeading, Icon, Text } from 'native-base';
import { TabNavigator, StackNavigator } from "react-navigation";
import * as tokenActions from '../../action/common/tokenActions';
import getTheme from './../themes/native-base-theme/components';
import material from './../themes/native-base-theme/variables/material';
import myTheme from './../themes/light.js';
import Design from './../design/Design';
import ProductGroup from './../products/ProductGroup';
import CartList from './../cart/CartList';
import InfoList from './../info/InfoList';
import ProductList from './../products/ProductList';
import ProductDetail from './../products/ProductDetail';
import ShoppingStep from './../shoppingstep/ShoppingStep';
import AddressDelivery from './../shoppingstep/AddressDelivery';
import AccessLayout from './../access/AccessLayout';

import UserDetail from './../info/UserDetail';

const HomeNavigator = TabNavigator({
  Design: { screen: Design },
  ProductGroup: { screen: ProductGroup },
  CartList: { screen: CartList },
  InfoList: { screen: InfoList },
},
{
    tabBarOptions:{
        showIcon : true,
        showLabel : false,
        style: {
            backgroundColor: '#d9534f',
        },
        indicatorStyle:{
            backgroundColor: '#fff',
        },
    },
    navigationOptions:{
        header: false
    }
});

const MainNavigator = StackNavigator({
  Main: { screen: HomeNavigator },
  ProductList: { screen: ProductList},
  ProductDetail: { screen: ProductDetail},
  ShoppingStep: { screen: ShoppingStep},
  AddressDelivery: { screen: AddressDelivery},
  AccessLayout: { screen: AccessLayout},
  UserDetail: { screen: UserDetail},
});


class Layout extends Component {

  constructor(props) {
      super(props);
      this.checkLogin = this.checkLogin.bind(this);
  }

  async checkLogin(){
    const {tokenActions} = this.props;
    let token = await AsyncStorage.getItem('utk');
    if(token) tokenActions.keepToken(token);
  }
  
  componentWillMount() {
      this.checkLogin();
  }
  
   render() {
        return (
            <StyleProvider style={getTheme(material)}>
                <MainNavigator/>
            </StyleProvider>
        );
   }
}

const mapStateToProps = (state) => {
    return {
    //   loading : state.signInReducer.loading,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        tokenActions: bindActionCreators(tokenActions, dispatch),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Layout);



