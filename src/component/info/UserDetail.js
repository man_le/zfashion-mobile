import React, {Component} from 'react';
import { connect } from 'react-redux';
import {StyleSheet} from 'react-native';
import { withNavigation } from 'react-navigation';
import * as R from './../../resources/R';

import { Button as ButtonReact, Image, ScrollView, Dimensions, AsyncStorage } from 'react-native';
import { 
        Card, CardItem, DeckSwiper, 
        View, Grid, Col, Icon, 
        Container, Content, List, ListItem, 
        Text, Left, Right, Thumbnail, 
        Body, Button, Header, Item, Input } from 'native-base';

class UserDetail extends Component{

    static navigationOptions = ({navigation}) => ({
        backgroundColor: '#d9534f',
        headerTitleStyle: {
            color: '#fff'
        },
        headerStyle: {
            backgroundColor: '#d9534f',
        },
        headerTintColor: '#fff',
        headerRight: 
            <Grid style={{marginTop:5, marginRight:10}}>
                <Col>
                    <Button transparent onPress={() => navigation.navigate('ShoppingStep')}>
                        <Icon style={{color:'#fff'}} name="cart" />
                    </Button>
                </Col>
                <Col>
                    <Button transparent>
                        <Icon style={{color:'#fff'}} name="share" />
                    </Button>
                </Col>
                <Col>
                    <Button transparent>
                        <Icon style={{color:'#fff'}} name="heart" />
                    </Button>
                </Col>
            </Grid>
    });

  constructor(props) {
      super(props);
  }

  render() {

    return (
        <Container>
            <Content>
                <Text>adfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdf</Text>
            </Content>
        </Container>
        
    );
  }
}


const mapStateToProps = (state) => {
    return {
      token : state.tokenReducer
    };
};

export default connect(
    mapStateToProps
)(UserDetail);
