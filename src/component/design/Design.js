import React, { Component } from 'react';
import { Image, WebView, StyleSheet, Dimensions } from 'react-native';
import { Button, Body, Left, Thumbnail, Card, CardItem, View, StyleProvider, Container, Content, Tab, Tabs, Header, TabHeading, Icon, Text } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import ResponsiveImage from 'react-native-responsive-image';
import HTMLView from 'react-native-htmlview';

export default class Design extends Component {

    static navigationOptions = {
        tabBarIcon: () => (
            <RNIcon color='#fff' size={25} name='camera-retro' />
        ),
    };

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <WebView automaticallyAdjustContentInsets={false}
                source={require('./../../assets/test/index.html')}
            />

        );
    }
}