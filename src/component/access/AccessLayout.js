import React, {Component} from 'react';
import { connect } from 'react-redux';
import {StyleSheet} from 'react-native';
import Access from './Access';
import { withNavigation } from 'react-navigation';
import * as R from './../../resources/R';
import UserDetail from './../info/UserDetail';

import { Button as ButtonReact, Image, ScrollView, Dimensions } from 'react-native';
import { 
        Card, CardItem, DeckSwiper, 
        View, Grid, Col, Icon, 
        Container, Content, List, ListItem, 
        Text, Left, Right, Thumbnail, 
        Body, Button, Header, Item, Input } from 'native-base';

class AccessLayout extends Component{

    static navigationOptions = ({navigation}) => ({
        title:'Đăng Nhập / Đăng Ký', 
        backgroundColor: '#d9534f',
        headerTitleStyle: {
            color: '#fff'
        },
        indicatorStyle :{
            backgroundColor: '#fff',
        },
        headerStyle: {
            backgroundColor: '#922420',
        },
        headerTintColor: '#fff'
    });

  constructor(props) {
      super(props);
  }

  render() {
    const {user} = this.props;
    
    if(user) return <UserDetail/>;

    return (
        <Access></Access>
    );
  }
}


const mapStateToProps = (state) => {
    return {
      user : state.signInReducer.user,
    };
};

export default connect(
    mapStateToProps
)(AccessLayout);