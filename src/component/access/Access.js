import React, { Component } from 'react';
import {AppRegistry} from 'react-native';
import { StyleProvider, Container, Content, Tab, Tabs, Header, TabHeading, Icon, Text } from 'native-base';
import { TabNavigator, StackNavigator } from "react-navigation";

import getTheme from './../themes/native-base-theme/components';
import material from './../themes/native-base-theme/variables/material';
import myTheme from './../themes/light.js';

import UserDetail from './../info/UserDetail';
import SignIn from './../access/SignIn';
import SignUp from './../access/SignUp';
import ForgotPassword from './../access/ForgotPassword';

const AccessNavigator = TabNavigator({
  SignIn: { screen: SignIn },
  SignUp: { screen: SignUp },
},
{
    tabBarOptions:{
        showIcon : false,
        showLabel : true,
        style: {
            backgroundColor: '#d9534f',
        },
        indicatorStyle:{
            backgroundColor: '#fff',
        },
    },
    navigationOptions:{
        header: false
    }
});

const MainNavigator = StackNavigator({
  Main: { screen: AccessNavigator },
  ForgotPassword: { screen: ForgotPassword }
});

export default class Access extends Component {
    render() {
        return (
            
                <StyleProvider style={getTheme(material)}>
                    <MainNavigator/>
                </StyleProvider>

        );
    }
}




