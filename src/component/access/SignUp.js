import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import lodash from 'lodash';
import { withNavigation } from 'react-navigation';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as R from './../../resources/R';
import CommonUtils from './../../util/CommonUtils';
import Center from './../common/Center';
import SpaceLine from './../common/SpaceLine';
import ItemInput from './../common/ItemInput';

import { Button as ButtonReact, Image, ScrollView, Dimensions } from 'react-native';
import { 
        Card, CardItem, DeckSwiper, 
        View, Grid, Col, Icon, 
        Container, Content, List, ListItem, 
        Text, Left, Right, Thumbnail, 
        Body, Button, Header, Item, Input,
        Form, Label
         } from 'native-base';

export default class SignUp extends Component{

    static navigationOptions = ({navigation}) => ({
        backgroundColor: '#d9534f',
        headerTitleStyle: {
            color: '#fff'
        },
        headerStyle: {
            backgroundColor: '#d9534f',
        },
        headerTintColor: '#fff',
        tabBarLabel: ()=>(
            <Text style={{color:'#fff', fontWeight:'bold'}}>{R.SIGN_UP.toUpperCase()}</Text>
        ),
    });

  constructor(props) {
      super(props);
      this.emailInputHandler = this.emailInputHandler.bind(this);
      this.submitLoginHandler = this.submitLoginHandler.bind(this);
      this.passwordInputHandler = this.passwordInputHandler.bind(this);
      this.state = {
                        validEmail:false, 
                        validPassword:false, 
                        validName:false}
  }

  emailInputHandler(e){
    if(!CommonUtils.isEmail(e)) this.setState({validEmail:false});
    else this.setState({validEmail:true});
    this.emailInput = e;
  }

  nameInputHandler(e){
    if(!e) this.setState({validName:false});
    else this.setState({validName:true});
    this.nameInput = e;
  }

  passwordInputHandler(e){
    if(!e) this.setState({validPassword:false});
    else this.setState({validPassword:true});
    this.passwordInput = e;
  }

  submitLoginHandler(e){
    console.log(this.emailInput);
  }

  render() {

    return (
        <Container>
            <Content>
                <Form>
                    <ItemInput label='Họ tên' valid={this.state.validName} onChangeText={e=>this.nameInputHandler(e)}/>
                    <ItemInput label='Email' valid={this.state.validEmail} onChangeText={e=>this.emailInputHandler(e)}/>
                    <ItemInput label='Password' valid={this.state.validPassword} onChangeText={e=>this.passwordInputHandler(e)} password={true}/>

                    <SpaceLine/>
                  
                    <ButtonReact
                        onPress={e=>{this.submitLoginHandler(e)}}
                        title={R.SIGN_UP}
                        color="#d9534f"/>

                    <SpaceLine/>

                </Form>
            </Content>
        </Container>
    );
  }
}