import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { NavigationActions } from 'react-navigation'
import {StyleSheet, Alert} from 'react-native';
import lodash from 'lodash';
import { withNavigation } from 'react-navigation';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as signInActions from '../../action/access/signInActions';
import * as errorActions from '../../action/common/errorActions';
import * as R from './../../resources/R';
import CommonUtils from './../../util/CommonUtils';
import * as errorType from './../../constant/errorType';
import Center from './../common/Center';
import SpaceLine from './../common/SpaceLine';
import ItemInput from './../common/ItemInput';
import SpinnerPage from './../common/SpinnerPage';
import UserDetail from './../info/UserDetail';

import { Button as ButtonReact, Image, ScrollView, Dimensions, AsyncStorage} from 'react-native';
import { 
        Card, CardItem, DeckSwiper, 
        View, Grid, Col, Icon, 
        Container, Content, List, ListItem, 
        Text, Left, Right, Thumbnail, 
        Body, Button, Header, Item, Input,
        Form, Label
         } from 'native-base';

class SignIn extends Component{

    static navigationOptions = ({navigation}) => ({
        backgroundColor: '#d9534f',
        headerTitleStyle: {
            color: '#fff'
        },
        headerStyle: {
            backgroundColor: '#d9534f',
        },
        headerTintColor: '#fff',
        tabBarLabel: ()=>(
            <Text style={{color:'#fff', fontWeight:'bold'}}>{R.SIGN_IN.toUpperCase()}</Text>
        ),
    });

  constructor(props) {
      super(props);
      this.emailInputHandler = this.emailInputHandler.bind(this);
      this.submitLoginHandler = this.submitLoginHandler.bind(this);
      this.passwordInputHandler = this.passwordInputHandler.bind(this);
      this.saveToken = this.saveToken.bind(this);
      this.state = {validEmail:false, validPassword:false}
  }

  emailInputHandler(e){
    if(!CommonUtils.isEmail(e)) this.setState({validEmail:false});
    else this.setState({validEmail:true});
    this.emailInput = e;
  }

  passwordInputHandler(e){
    if(!e) this.setState({validPassword:false});
    else this.setState({validPassword:true});
    this.passwordInput = e;
  }

  submitLoginHandler(e){
    const {user, accessToken, errorActions, signInActions, error} = this.props;
    const {navigate} = this.props.navigation;

    signInActions.signIn(this.emailInput, this.passwordInput);

    // navigate('UserDetail');


// const navigateAction = NavigationActions.navigate({

//   routeName: 'UserDetail',

//   params: {},

//   action: NavigationActions.navigate({ routeName: 'UserDetail'})
// })

// this.props.navigation.dispatch(navigateAction)

// this.props.navigation.navigate()

// this.props.navigation.goBack()


// const backAction = NavigationActions.back({
//   key: null
// })
// this.props.navigation.dispatch(backAction)







  }

  componentWillMount() {
      const {user} = this.props;
  }

 async saveToken(){
      const {accessToken} = this.props;
      await AsyncStorage.setItem('utk', accessToken);
  }

  render() {
    const {user, loading, errorActions, error} = this.props;
    

    console.log('============');
    console.log(error);
    if(error.statusCode == errorType.WRONG_USER_PASS) 
        Alert.alert('',
            'Wrong Email Or Password.',[{
                    text:'Đóng', 
                    onPress:()=>errorActions.resetError()
                }]
        )
    if(loading) return <SpinnerPage/>;

    return (
        <Container>
            <Content>
                <View>
                <Form>
                    <ItemInput label='Email' valid={this.state.validEmail} onChangeText={e=>this.emailInputHandler(e)}/>
                    <ItemInput label='Password' valid={this.state.validPassword} onChangeText={e=>this.passwordInputHandler(e)} password={true}/>

                    <SpaceLine/>
                  
                    <ButtonReact
                        onPress={e=>{this.submitLoginHandler(e)}}
                        title={R.SIGN_IN}
                        color="#d9534f"/>

                    <SpaceLine/>

                    <Center>
                            <Text style={{color:'#d9534f'}} ellipsizeMode='middle'>Quên mật khẩu ?</Text>
                    </Center>
                    
                    <SpaceLine height={20}/>
                    <Center>
                            <Text style={{color:'#737373'}} ellipsizeMode='middle'>Hoặc đăng nhập bằng</Text>
                    </Center>
                    <SpaceLine/>
                    <Center>
                        <View style={{flex: 1, flexDirection: 'row'}}>
                            <Button style={{marginRight:5, width:100}}>
                                <RNIcon color='#fff' size={25} name='facebook' />
                            </Button>
                            <Button style={{width:100}} danger>
                                <RNIcon color='#fff' size={25} name='google-plus' />
                            </Button>
                        </View>
                    </Center>

                </Form>
                </View>
            </Content>
        </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {
      loading : state.signInReducer.loading,
      user : state.signInReducer.user,
      accessToken : state.signInReducer.accessToken,
      error : state.errorReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        signInActions: bindActionCreators(signInActions, dispatch),
        errorActions: bindActionCreators(errorActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SignIn);
