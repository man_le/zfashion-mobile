import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import { 
        Button as ButtonReact, 
        Text as TextRN, 
        Image, ScrollView, Dimensions, 
        View as ViewRN,
        Modal, TouchableHighlight, StyleSheet
} from 'react-native';

import { connect } from 'react-redux';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as productListActions from '../../action/products/productListActions';
import * as componentType from './../../constant/componentType';
import ZListItem from './../common/ListItem';
import ZModal from './../common/Modal';
import Access from './../access/Access';
import * as R from './../../resources/R';
import { View, Separator, Grid, Col, Icon, Container, Content, List, ListItem, Text, Left, Right, Thumbnail, Body, Button, Header, Item, Input } from 'native-base';

class SortListType extends Component{

  static navigationOptions = {
    tabBarIcon: () => (
      <Icon style={{color: '#fff'}} name="person" />
    ),
  };

    constructor(props) {
        super(props);
        this.state = {
            modalSignInVisible : false
        }
    }

  render() {
    const {user, productList} = this.props;

    console.log('----USER-----');
    console.log(user);

    var styles = StyleSheet.create({
        list:{backgroundColor:'#fff'},
        separator:{height:2}
    });

    const styles_list = StyleSheet.flatten(styles.list);
    const styles_separator = StyleSheet.flatten(styles.separator);

    return(
        <Container>
            <Content>
                    <List style={styles_list}>
                        <ZListItem icon='info-circle' label='Giá tăng dần' onPress={() => this.props.navigation.navigate('ProductDetail', {product:R.SWEATER})}></ZListItem>
                        <Separator style={styles_separator}/>

                        <ZListItem icon='address-book' label='Giá giảm dần' onPress={() => this.props.navigation.navigate('ProductDetail', {product:R.SWEATER})}></ZListItem>
                        <Separator style={styles_separator}/>
                    
                        <ZListItem icon='id-card-o' label='Bán chạy nhất' onPress={() => this.props.navigation.navigate('ProductDetail', {product:R.SWEATER})}></ZListItem>
                        <Separator style={styles_separator}/>

                        <ZListItem icon='heart' label='Xem nhiều nhất' onPress={() => this.props.navigation.navigate('ProductDetail', {product:R.SWEATER})}></ZListItem>
                        <Separator style={styles_separator}/>

                    </List>

                    {/*Sign In/Up Modal*/}
                    <ZModal 
                        title = 'Đăng Nhập / Đăng Ký'
                        visible={this.state.modalSignInVisible && !user}
                        onRequestClose={() => {this.setState({modalSignInVisible:false})}}>
                            <Access/>
                    </ZModal>
            </Content>
        </Container>
    );
  }
}




const mapStateToProps = (state) => {
    return {
        user : state.signInReducer.user,
        productList: state.productListReducer
    };
};

const mapDispatchToProps = (dispatch) => {
       return {
            actions: bindActionCreators(productListActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(SortListType)


