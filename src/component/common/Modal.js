import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import { 
        Button as ButtonReact, 
        Text as TextRN, 
        Image, ScrollView, Dimensions, 
        View as ViewRN,
        Modal, TouchableHighlight, StyleSheet
} from 'react-native';

import { connect } from 'react-redux';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as productListActions from '../../action/products/productListActions';
import * as componentType from './../../constant/componentType';
import ZListItem from './../common/ListItem';
import Access from './../access/Access';
import * as R from './../../resources/R';
import { View, Separator, Grid, Col, Icon, Container, Content, List, ListItem, Text, Left, Right, Thumbnail, Body, Button, Header, Item, Input } from 'native-base';

class ZModal extends Component{

    constructor(props) {
        super(props);
    }

  render() {
    return(
            <Modal
                animationType={"slide"}
                transparent={false}
                visible={this.props.visible}
                onRequestClose={() => this.props.onRequestClose()}
                >
                <View style={{height:50}}>
                <View style={{flex: 1, flexDirection: 'row', backgroundColor:this.props.bkColor?this.props.bkColor:'#922420'}}>
                    
                    <View>
                        <Button onPress={() => this.props.onRequestClose()} transparent style={{marginRight:5}}>
                            <RNIcon color='#fff' size={20} name='close' />
                        </Button>
                    </View>

                    <View>
                        <Text style={{marginTop:12, color:'#fff', fontWeight:'bold'}}>{this.props.title}</Text>
                    </View>
                    
                </View>
                </View>
                {this.props.children}
            </Modal>
    );
  }
}




const mapStateToProps = (state) => {
    return {
        // todo
    };
};

const mapDispatchToProps = (dispatch) => {
       return {
           // todo
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ZModal)


