import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import lodash from 'lodash';
import { withNavigation } from 'react-navigation';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as R from './../../resources/R';
import CommonUtils from './../../util/CommonUtils';
import Center from './../common/Center';
import SpaceLine from './../common/SpaceLine';

import { Button as ButtonReact, Image, ScrollView, Dimensions } from 'react-native';
import { 
        Card, CardItem, DeckSwiper, 
        View, Grid, Col, Icon, 
        Container, Content, List, ListItem, 
        Text, Left, Right, Thumbnail, 
        Body, Button, Header, Item, Input,
        Form, Label
         } from 'native-base';

export default class ItemInput extends Component{

  constructor(props) {
      super(props);
      this.state = {validEmail:false, validPassword:false}
  }

  render() {

    return (
        <Item error={!this.props.valid} success={this.props.valid} floatingLabel last>
            <Label style={{marginLeft:20}}>{this.props.label}</Label>
            <Input onChangeText={e=>this.props.onChangeText(e)} secureTextEntry={this.props.password?this.props.password:false} style={{marginLeft:10}} />
                <Icon name={!this.props.valid?'close-circle':'checkmark-circle'}/>
        </Item>
    );
  }
}

