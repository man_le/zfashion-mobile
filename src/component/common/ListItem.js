import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import {StyleSheet, Image} from 'react-native'
import * as productActions from '../../action/productActions';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as R from './../../resources/R';
import lodash from 'lodash';
import { Left, Right, Thumbnail, Separator, Body, List, ListItem, Button, Item, Input, View, StyleProvider, Container, Content, Tab, Tabs, Header, TabHeading, Icon, Text } from 'native-base';

class ZListItem extends Component{

  constructor(props) {
      super(props);

      this.onPressHandler = this.onPressHandler.bind(this);
      this.state = {preventClick:false};
      this.renderContent = this.renderContent.bind(this);

  }
  
  onPressHandler(){
      if(!this.state.preventClick){
          this.setState({preventClick:true});
          this.props.onPress();
          setTimeout(()=>{
             this.setState({preventClick:false});
          },2000);
      }
  }

   renderContent(){
        const {style, children, state, actions, icon, label} = this.props;
        
        if(children){
            return(
                <ListItem style={style?style:null} button onPress={() => this.onPressHandler()}>
                    {children}
                </ListItem>
            )
        }else{
            return(
                <ListItem style={style?style:null} icon onPress={() => this.onPressHandler()}>
                    <Left>
                        <RNIcon color='#737373' size={25} name={icon} />
                    </Left>
                    <Body>
                        <Text style={{color:'#737373'}}>{label}</Text>
                    </Body>
            </ListItem>
            )
        }
   }
  
    render() {
        const {style, children, state, actions, icon, label} = this.props;
        
        return(
            this.renderContent()
        );
    }
}

export default ZListItem;












