import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import { withNavigation } from 'react-navigation';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as R from './../../resources/R';

import { Button as ButtonReact, Image, ScrollView, Dimensions } from 'react-native';
import { 
        Card, CardItem, DeckSwiper, 
        View, Grid, Col, Icon, 
        Container, Content, List, ListItem, 
        Text, Left, Right, Thumbnail, 
        Body, Button, Header, Item, Input,
        Form, Label
         } from 'native-base';

export default class Center extends Component{


  constructor(props) {
      super(props);
  }

 
  render() {

    return (
        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            alignItems: 'center',
                    }}>
            {this.props.children}
        </View>
    );
  }
}