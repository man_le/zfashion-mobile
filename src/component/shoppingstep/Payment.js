import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet} from 'react-native';
import ShoppingCartItem from './ShoppingCartItem';
import { connect } from 'react-redux';
import * as productListActions from '../../action/products/productListActions';
import * as componentType from './../../constant/componentType';
import * as R from './../../resources/R';
import { 
        Button as ButtonReact, 
        Text as TextRN, 
        Image, ScrollView, Dimensions, 
        View as ViewRN,
        Modal, TouchableHighlight
} from 'react-native';

import { View, Separator, Grid, Col, Icon, Container, 
    Content, List, ListItem, Text, Left, Right, 
    Thumbnail, Body, Button, Header, Item, Input 
} from 'native-base';

class Payment extends Component{

    static navigationOptions = ({navigation}) => ({
        backgroundColor: '#d9534f',
        title: R.SHOPPING_CART+' ('+3+')',
        headerTitleStyle: {
            color: '#fff'
        },
        headerStyle: {
            backgroundColor: '#d9534f',
        },
        headerTintColor: '#fff'
    });


    constructor(props) {
        super(props);
        this.doLayout = this.doLayout.bind(this);
        this.setModalVisible = this.setModalVisible.bind(this);
        const {width, height} = Dimensions.get('window');
        this.state={
            width : width,
            height : height,
            modalVisible: false
        }
    }

  doLayout(){
      const {width, height} = Dimensions.get('window');
      this.setState({width:width, height:height});
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    const {productList} = this.props;

    var styles = StyleSheet.create({
        list:{backgroundColor:'#fff'},
        separator:{height:2}
    });

    const styles_list = StyleSheet.flatten(styles.list);
    const styles_separator = StyleSheet.flatten(styles.separator);

    return(
        <Container onLayout={()=>{this.doLayout()}}>
            <Content style={{backgroundColor:'#fff'}}>
                <Text>Payment</Text>
            </Content>
        </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        productList: state.productListReducer
    };
};

const mapDispatchToProps = (dispatch) => {
       return {
            actions: bindActionCreators(productListActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Payment)
