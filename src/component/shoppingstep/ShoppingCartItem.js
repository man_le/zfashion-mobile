import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {StyleSheet, Image, 
    Button as ButtonReact,
    Text as TextRN,
    View as ViewRN,
} from 'react-native'
import * as productActions from '../../action/productActions';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as R from './../../resources/R';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import { 
    Right, Thumbnail, Separator, Body, List, 
    ListItem, Button, Item, Input, View, 
    StyleProvider, Container, Content, Tab, Tabs, 
    Header, TabHeading, Icon, Text,
} from 'native-base';

class ShoppingCartItem extends Component{

  static navigationOptions = {
    tabBarIcon: () => (
      <Icon style={{color: '#fff'}} name="grid" />
    ),
  };
  
  constructor(props) {
      super(props);
      this.onPressHandler = this.onPressHandler.bind(this);
      this.state = {preventClick:false};
  }
  
  onPressHandler(){
    if(!this.state.preventClick){
        this.setState({preventClick:true});
        this.props.onPress();
        setTimeout(()=>{
           this.setState({preventClick:false});
        },2000);
    }
}

    render() {
        const {state, actions} = this.props;
        
        var styles = StyleSheet.create({
            oldPrice:{color:'#ccc', textDecorationLine:'line-through'},
            newPrice:{color:'#d9534f', fontSize:15},
        });

        const styles_oldPrice = StyleSheet.flatten(styles.oldPrice);
        const styles_newPrice = StyleSheet.flatten(styles.newPrice);
        const styles_separator = StyleSheet.flatten(styles.separator);
        
        return(
            <ListItem>
                <Thumbnail square size={100} source={require('./../../assets/imgs/sweater.jpg')} />
                <Body>
                    <Text>Ao Len</Text>
                    <Text note style={styles_oldPrice}>202.000 đ</Text>
                    <Text note style={styles_newPrice}>102.000 đ</Text>
                    <Button style={{marginTop:10, marginLeft:9, height:20}} danger onPress={() => this.onPressHandler()}>
                        <TextRN style={{fontSize:10, color:'#fff'}}>Chi tiết</TextRN>
                    </Button>
                </Body>
                <Right>
                    <ViewRN style={{height:125}}>
                        <Grid> 
                            <Button transparent style={{height:30}}>
                                <RNIcon style={{marginBottom:15}} color='#d9534f' size={20} name='close' />
                            </Button>
                        </Grid>

                        <Grid> 
                            <Button style={{height:30}} danger >
                                <RNIcon style={{marginTop:5}} color='#fff' size={20} name='sort-up' />
                            </Button>
                        </Grid>
                        <Grid style={{marginTop:-2, backgroundColor:'#e38582', width:43}}>
                            <ViewRN style={{width: 43, marginTop:5}}>
                                <TextRN style={{fontWeight:'bold', height:25, color:'#fff', textAlign:'center'}}>
                                    30
                                </TextRN>
                            </ViewRN>
                        </Grid>
                        <Grid>
                            <Button style={{height:30}} danger >
                                <RNIcon style={{marginTop:-9}} color='#fff' size={20} name='sort-down' />
                            </Button>
                        </Grid>

                    </ViewRN>
                </Right>
            </ListItem>
        );
    }
}

ShoppingCartItem.propTypes = {
    onPress : PropTypes.func
};


const mapStateToProps = (state) => {
    return {
      state : state.productReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(productActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShoppingCartItem);
