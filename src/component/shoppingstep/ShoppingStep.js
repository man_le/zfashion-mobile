import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet} from 'react-native';
import ShoppingCart from './ShoppingCart';
import { connect } from 'react-redux';
import * as productListActions from '../../action/products/productListActions';
import * as componentType from './../../constant/componentType';
import * as R from './../../resources/R';
import { 
        Button as ButtonReact, 
        Text as TextRN, 
        Image, ScrollView, Dimensions, 
        View as ViewRN,
        Modal, TouchableHighlight
} from 'react-native';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import { View, Separator, Grid, Col, Icon, Container, 
    Content, List, ListItem, Text, Left, Right, 
    Thumbnail, Body, Button, Header, Item, Input 
} from 'native-base';

class ShoppingStep extends Component{

    static navigationOptions = ({navigation}) => ({
        backgroundColor: '#d9534f',
        title: R.SHOPPING_CART+' ('+3+')',
        headerTitleStyle: {
            color: '#fff'
        },
        headerStyle: {
            backgroundColor: '#d9534f',
        },
        headerTintColor: '#fff'
    });


    constructor(props) {
        super(props);
        this.doLayout = this.doLayout.bind(this);
        const {width, height} = Dimensions.get('window');
        this.state={
            width : width,
            height : height
        };
    }

  doLayout(){
      const {width, height} = Dimensions.get('window');
      this.setState({width:width, height:height});
  }

  render() {
    // const {} = this.props;
    return(
        <Container onLayout={()=>{this.doLayout()}}>
            <Content style={{backgroundColor:'#fff'}}>

                <Grid style={{marginTop:10}}>
                    <Col style={{height: 50 }}>
                        <Button style={{width:50, position:'absolute', right:0}} danger>
                            <RNIcon color='#fff' size={20} name='shopping-cart' />
                        </Button>
                    </Col>
                    <Col style={{marginTop:19, backgroundColor: '#e6e6e6', height: 5 }}></Col>
                    <Col style={{width:50, height: 50 }}>
                        <Button style={{width:50}} danger>
                            <RNIcon color='#fff' size={20} name='location-arrow' />
                        </Button>
                    </Col>
                    <Col style={{marginTop:19, backgroundColor: '#e6e6e6', height: 5 }}></Col>
                    <Col style={{height: 50 }}>
                        <Button style={{width:50, position:'absolute', left:0}} danger>
                            <RNIcon style={{marginLeft:4}} color='#fff' size={20} name='dollar' />
                        </Button>
                    </Col>
                </Grid>

                <ShoppingCart/>
            </Content>
        </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        // productList: state.productListReducer
    };
};

const mapDispatchToProps = (dispatch) => {
       return {
            // actions: bindActionCreators(productListActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShoppingStep)
