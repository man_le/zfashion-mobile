import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import {StyleSheet, Image} from 'react-native'
import * as productActions from '../../action/productActions';
import { Col, Row, Grid } from 'react-native-easy-grid';
import * as R from './../../resources/R';
import { Left, Right, Thumbnail, Separator, Body, List, ListItem, Button, Item, Input, View, StyleProvider, Container, Content, Tab, Tabs, Header, TabHeading, Icon, Text } from 'native-base';

class CartItem extends Component{

  constructor(props) {
      super(props);
  }
  
    render() {
        const {state, actions, icon, label} = this.props;
        
        var styles = StyleSheet.create({
            oldPrice:{color:'#ccc', textDecorationLine:'line-through'},
            newPrice:{color:'#d9534f', fontSize:15},
        });

        const styles_oldPrice = StyleSheet.flatten(styles.oldPrice);
        const styles_newPrice = StyleSheet.flatten(styles.newPrice);
        const styles_separator = StyleSheet.flatten(styles.separator);
        
        return(
            <ListItem icon onPress={() => this.props.onPress()}>
                <Left>
                    <RNIcon color='#737373' size={25} name={icon} />
                </Left>
                <Body>
                    <Text style={{color:'#737373'}}>{label}</Text>
                </Body>
            </ListItem>
        );
    }
}

const mapStateToProps = (state) => {
    return {
      state : state.productReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(productActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CartItem);












