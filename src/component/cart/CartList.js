import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet} from 'react-native';
import ZListItem from './../common/ListItem';
import { connect } from 'react-redux';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as productListActions from '../../action/products/productListActions';
import * as componentType from './../../constant/componentType';
import * as R from './../../resources/R';
import { Separator, Grid, Col, Icon, Container, Content, List, ListItem, Text, Left, Right, Thumbnail, Body, Button, Header, Item, Input } from 'native-base';

class CartList extends Component{

  static navigationOptions = {
    tabBarIcon: () => (
      <RNIcon color='#fff' size={25} name='shopping-cart' />
    ),
  };


    constructor(props) {
        super(props);
    }

  render() {
    const {productList} = this.props;

    var styles = StyleSheet.create({
        list:{backgroundColor:'#fff'},
        separator:{height:2}
    });

    const styles_list = StyleSheet.flatten(styles.list);
    const styles_separator = StyleSheet.flatten(styles.separator);

    return(
        <Container>
            <Content>
                <Content>
                    <List style={styles_list}>
                        <ZListItem icon='shopping-cart' label='Giỏ hàng của bạn' onPress={() => this.props.navigation.navigate('ShoppingStep')}></ZListItem>
                        <Separator style={styles_separator}/>

                        <ZListItem icon='hourglass-half' label='Kiểm tra đơn hàng' onPress={() => this.props.navigation.navigate('ProductDetail', {product:R.SWEATER})}></ZListItem>
                        <Separator style={styles_separator}/>
                    
                    </List>
                </Content>
            </Content>
        </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        productList: state.productListReducer
    };
};

const mapDispatchToProps = (dispatch) => {
       return {
            actions: bindActionCreators(productListActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CartList)
