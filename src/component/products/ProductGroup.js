import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import {StyleSheet, Image} from 'react-native'
import * as productActions from '../../action/productActions';
import { Col, Row, Grid } from 'react-native-easy-grid';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import ZListItem from './../common/ListItem';
import * as R from './../../resources/R';
import { Separator, Body, List, ListItem, Button, Item, Input, View, StyleProvider, Container, Content, Tab, Tabs, Header, TabHeading, Icon, Text } from 'native-base';

class ProductGroup extends Component{

  static navigationOptions = {
    tabBarIcon: () => (
      <RNIcon color='#fff' size={25} name='list-alt' />
    ),
  };
  
    render() {
        const {state, actions} = this.props;
        
        var styles = StyleSheet.create({
            search:{backgroundColor: '#fff'},
            grid:{backgroundColor:'#ccc', height: 202},
            col:{backgroundColor: '#fff', height: 200},
            text:{color:'#ccc', fontWeight:'bold', fontSize: 25},

            list:{backgroundColor:'#fff'},
            listItem:{height:200},
            image:{marginLeft:-20},
            separator:{height:2}
        });

        const styles_list = StyleSheet.flatten(styles.list);
        const styles_listItem = StyleSheet.flatten(styles.listItem);
        const styles_text = StyleSheet.flatten(styles.text);
        const styles_image = StyleSheet.flatten(styles.image);
        const styles_separator = StyleSheet.flatten(styles.separator);
        
        return(
            <Container>
                <Header style={{backgroundColor: '#fff'}} searchBar rounded>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Search" />
                    </Item>
                </Header>
                <Content>

            <List style={styles_list}>

                <ZListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductList', {product:R.SWEATER})}>
                    <Image style={styles_image} source={require('./../../assets/imgs/sweater.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.SWEATER}</Text>
                    </Body>
                </ZListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/shirt.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.SHIRT}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/tshirt.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.TSHIRT}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/dress.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.DRESS}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/culottes.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.CULOTTES}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/jean.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.JEAN}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/kaki.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.KAKI}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/short.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.SHORT}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/skirt.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.SKIRT}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/hat.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.HAT}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/glasses.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.GLASSES}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>

                <ListItem style={styles_listItem} onPress={() => this.props.navigation.navigate('ProductsDetail')}>
                    <Image style={styles_image} source={require('./../../assets/imgs/bag.jpg')} />
                    <Body>
                        <Text style={styles_text}>{R.BAG}</Text>
                    </Body>
                </ListItem>
                <Separator style={styles_separator}/>
            </List>

                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
      state : state.productReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(productActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductGroup);