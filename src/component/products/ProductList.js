import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {StyleSheet} from 'react-native';
import ProductItem from './ProductItem';
import { connect } from 'react-redux';
import ZModal from './../common/Modal';
import SortListType from './../common/SortListType';
import Design from './../design/Design';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as productListActions from '../../action/products/productListActions';
import * as componentType from './../../constant/componentType';
import * as R from './../../resources/R';
import { Separator, Grid, Col, Icon, Container, Content, List, ListItem, Text, Left, Right, Thumbnail, Body, Button, Header, Item, Input } from 'native-base';

class ProductList extends Component{

    constructor(props) {
        super(props);
        this.state = {
            modalPreviewVisible : false,
            modalSortTypeVisible: false,
            preventClick:false
        }
        this.onPressShoppingCartHandler = this.onPressShoppingCartHandler.bind(this);
        this.onPressSortTypeHandler = this.onPressSortTypeHandler.bind(this);
    }

    static navigationOptions = ({navigation}) => ({
        backgroundColor: '#d9534f',
        title: navigation.state.params.product,
        headerTitleStyle: {
            color: '#fff'
        },
        headerStyle: {
            backgroundColor: '#d9534f',
        },
        headerTintColor: '#fff',
        headerRight: 
            <Grid style={{marginTop:5, marginRight:10}}>
                <Col>
                    <Button transparent onPress={()=> navigation.state.params.onPressSortTypeHandler()}>
                        <RNIcon color='#fff' size={22} name='filter' />
                    </Button>
                </Col>
                <Col>
                    <Button transparent onPress={() => navigation.state.params.onPressShoppingCartHandler(navigation)}>
                        <Icon style={{color:'#fff'}} name="cart" />
                    </Button>
                </Col>
            </Grid>
    });

    onPressShoppingCartHandler(navigation){
        if(!this.state.preventClick){
            this.setState({preventClick:true});
            navigation.navigate('ShoppingStep');
            setTimeout(()=>{
                this.setState({preventClick:false});
            },2000);
        }
    }

  onPressSortTypeHandler(){
    this.setState({modalSortTypeVisible:true});
  }

  componentDidMount() {
    this.props.navigation.setParams({
        onPressShoppingCartHandler: this.onPressShoppingCartHandler,
        onPressSortTypeHandler : this.onPressSortTypeHandler
    })
  }
  

  render() {
    const {productList} = this.props;

    var styles = StyleSheet.create({
        list:{backgroundColor:'#fff'},
        separator:{height:2}
    });

    const styles_list = StyleSheet.flatten(styles.list);
    const styles_separator = StyleSheet.flatten(styles.separator);

    return(
        <Container>
            <Header style={{backgroundColor: '#fff'}} searchBar rounded>
                <Item>
                    <Icon name="ios-search" />
                    <Input placeholder="Search" />
                </Item>
            </Header>
            <Content>
                <Content>
                    <List style={styles_list}>
                        <ProductItem onPressPreview={()=> this.setState({modalPreviewVisible:true})} onPress={() => this.props.navigation.navigate('ProductDetail', {product:R.SWEATER})}></ProductItem>
                        <Separator style={styles_separator}/>
                    </List>
                </Content>
            </Content>

            {/* Modal For Preview */}
            <ZModal 
                bkColor  = '#d9534f'
                title = 'Xem Trước'
                visible={this.state.modalPreviewVisible}
                onRequestClose={() => {this.setState({modalPreviewVisible:false})}}>
                    <Design/>
            </ZModal>

            {/* Modal For Sort Type */}
            <ZModal 
                bkColor = '#d9534f'
                title = 'Sắp Xếp Theo'
                visible={this.state.modalSortTypeVisible}
                onRequestClose={() => {this.setState({modalSortTypeVisible:false})}}>
                    <SortListType/>
            </ZModal>

        </Container>
    );
  }
}

const mapStateToProps = (state) => {
    return {
        productList: state.productListReducer
    };
};

const mapDispatchToProps = (dispatch) => {
       return {
            actions: bindActionCreators(productListActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductList)
