import React, {Component} from 'react';
import { connect } from 'react-redux';
import {StyleSheet} from 'react-native';
import ProductItem from './ProductItem';
import { withNavigation } from 'react-navigation';
import * as R from './../../resources/R';
import Access from './../access/Access';

import { Button as ButtonReact, Image, ScrollView, Dimensions, AsyncStorage } from 'react-native';
import { 
        Card, CardItem, DeckSwiper, 
        View, Grid, Col, Icon, 
        Container, Content, List, ListItem, 
        Text, Left, Right, Thumbnail, 
        Body, Button, Header, Item, Input } from 'native-base';

class ProductDetail extends Component{

    static navigationOptions = ({navigation}) => ({
        backgroundColor: '#d9534f',
        headerTitleStyle: {
            color: '#fff'
        },
        headerStyle: {
            backgroundColor: '#d9534f',
        },
        headerTintColor: '#fff',
        headerRight: 
            <Grid style={{marginTop:5, marginRight:10}}>
                <Col>
                    <Button transparent onPress={() => navigation.navigate('ShoppingStep')}>
                        <Icon style={{color:'#fff'}} name="cart" />
                    </Button>
                </Col>
                <Col>
                    <Button transparent>
                        <Icon style={{color:'#fff'}} name="share" />
                    </Button>
                </Col>
                <Col>
                    <Button transparent>
                        <Icon style={{color:'#fff'}} name="heart" />
                    </Button>
                </Col>
            </Grid>
    });

  constructor(props) {
      super(props);
      this.doLayout = this.doLayout.bind(this);
      const {width, height} = Dimensions.get('window');
      this.checkLogin = this.checkLogin.bind(this);
      this.state={
          width : width,
          height : height,
          loggedIn: null
      }
  }

  doLayout(){
      const {width, height} = Dimensions.get('window');
      this.setState({width:width, height:height});
  }

  async checkLogin(){
    let token = await AsyncStorage.getItem('utk');

    console.log('TOKEN FROM ASYNCSTORAGE');
    console.log(token);

    if(token) this.setState({loggedIn:true});
    else this.setState({loggedIn:false})
  }
 
  componentWillMount() {
    //   this.checkLogin();
  }
  

  render() {
    const {token} = this.props;
    
    const cards = [
    {
        text: 'Card One',
        name: 'One',
        image: require('./../../assets/imgs/sweater.jpg'),
    },
        {
        text: 'Card One',
        name: 'One',
        image: require('./../../assets/imgs/sweater.jpg'),
    },
        {
        text: 'Card One',
        name: 'One',
        image: require('./../../assets/imgs/sweater.jpg'),
    },
    ];

    console.log(token);

    return (
        !token.value
        ?
        <Access/>
        :
        <Container onLayout={()=>{this.doLayout()}}>
            <Content style={{backgroundColor:'#fff'}}>
                <ScrollView style={{height:(this.state.height-95)}}>
                    <View style={{height:201, borderBottomColor:'#ccc', borderBottomWidth:1}}>
                        <DeckSwiper style={{}}
                            dataSource={cards}
                            renderItem={item =>
                                <View style={{alignItems: 'center'}}>
                                    <Image style={{ width:200, height: 200}} source={item.image} />
                                </View>

                            }
                        />
                    </View>
                    <Text style={{height:this.state.height}}>adfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdfadfasdfasdfasdfasdfasdf</Text>
                </ScrollView>

                <View style={{flexDirection:'column', alignItems:'center'}}>
                        <ButtonReact
                            onPress={()=>{console.log('sss')}}
                            title={R.ADD_TO_CART}
                            color="#d9534f"/>
                </View>
                
            </Content>
        </Container>
        
    );
  }
}

const mapStateToProps = (state) => {
    return {
      token : state.tokenReducer
    };
};

export default connect(
    mapStateToProps
)(ProductDetail);
