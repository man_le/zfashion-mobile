import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {StyleSheet, Image} from 'react-native'
import * as productActions from '../../action/productActions';
import { Col, Row, Grid } from 'react-native-easy-grid';
import RNIcon from 'react-native-vector-icons/FontAwesome';
import * as R from './../../resources/R';
import { Right, Thumbnail, Separator, Body, List, ListItem, Button, Item, Input, View, StyleProvider, Container, Content, Tab, Tabs, Header, TabHeading, Icon, Text } from 'native-base';

class ProductItem extends Component{

  static navigationOptions = {
    tabBarIcon: () => (
      <Icon style={{color: '#fff'}} name="grid" />
    ),
  };
  
  constructor(props) {
      super(props);
      this.onPressHandler = this.onPressHandler.bind(this);
      this.onPressPreview = this.onPressPreview.bind(this);
      this.state = {preventClick:false};
  }
  
    onPressPreview(){
        this.props.onPressPreview();
    }

    onPressHandler(){
        if(!this.state.preventClick){
            this.setState({preventClick:true});
            this.props.onPress();
            setTimeout(()=>{
                this.setState({preventClick:false});
            },2000);
        }
    }

    render() {
        const {state, actions} = this.props;
        
        var styles = StyleSheet.create({
            list:{backgroundColor:'#fff'},
            oldPrice:{color:'#ccc', textDecorationLine:'line-through'},
            newPrice:{color:'#d9534f', fontSize:15},
        });

        const styles_list = StyleSheet.flatten(styles.list);
        const styles_oldPrice = StyleSheet.flatten(styles.oldPrice);
        const styles_newPrice = StyleSheet.flatten(styles.newPrice);
        const styles_separator = StyleSheet.flatten(styles.separator);
        
        return(
            <List style={styles_list}>
                <ListItem button onPress={() => this.onPressHandler()}>
                    <Thumbnail square size={80} source={require('./../../assets/imgs/sweater.jpg')} />
                    <Body>
                        <Text>Ao Len </Text>
                        <Text note style={styles_oldPrice}>202.000 đ</Text>
                        <Text note style={styles_newPrice}>102.000 đ</Text>
                    </Body>
                    <Right>
                        <Button transparent danger onPress={() => this.onPressPreview()}>
                            <RNIcon color='#d9534f' size={27} name='camera-retro' />
                        </Button>
                    </Right>
                </ListItem>
            </List>
        );
    }
}


ProductItem.propTypes = {
    onPress : PropTypes.func,
    onPressPreview: PropTypes.func
};


const mapStateToProps = (state) => {
    return {
      state : state.productReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(productActions, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProductItem);








