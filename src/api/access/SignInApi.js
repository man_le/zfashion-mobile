import ajax from 'superagent';
import { Observable } from 'rxjs/Observable'
import * as urlConstants from './../../constant/urlConstants';
import * as commonType from './../../constant/commonType';

class UserApi {

  static signIn(email, password) {
    return Observable.ajax.post(urlConstants.SIGN_IN, {email,password}, commonType.CT_APP_JSON);
  }
}
export default UserApi;