export const JACKET = 'Áo Khoác';
export const SWEATER = 'Áo Len';
export const SHIRT = 'Áo Sơ Mi';
export const TSHIRT = 'Áo Thun';
export const DRESS = 'Đầm';
export const CULOTTES = 'Quần Culottes';
export const JEAN = 'Quần Jean';
export const KAKI = 'Quần Kaki';
export const SHORT = 'Quần Short';
export const SKIRT = 'Váy';
export const HAT = 'Nón';
export const GLASSES = 'Mắt Kính';
export const BAG = 'Túi Xách';

export const ADD_TO_CART = 'Thêm Vào Giỏ Hàng';
export const CONTINUE_FOR_SUBMIT = 'Tiến Hành Đặt Hàng';
export const SHOPPING_CART = 'Giỏ hàng';

export const SIGN_IN = 'Đăng nhập';
export const SIGN_UP = 'Đăng ký';
