export const MSG = 'MSG';


export const SIGN_IN = 'SIGN_IN';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const ERROR = 'ERROR';
export const RESET_ERROR = 'RESET_ERROR';
export const KEEP_TOKEN = 'KEEP_TOKEN';


export const PRODUCT = 'PRODUCT';
export const PRODUCT_LIST = 'PRODUCT_LIST';
export const COMMON = 'COMMON';

