import * as types from './../types';
import * as componentType from './../../constant/componentType';

const state = {
  type: null,
  loading: false,
  user:null,
  accessToken:null,
}

export const signIn = (email, password) => ({ type: types.SIGN_IN, loading:true, email, password});
export function signInSuccess(res) {
    if(res) res = res.response;
    return Object.assign({}, defaultState, {
        type: types.SIGN_IN_SUCCESS,
        loading:false,
        user: res.user,
        accessToken: res.accessToken
    });
}



export function defaultState(){
  return state;
}