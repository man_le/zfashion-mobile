import * as types from './../types';

const state = {
    value: null
}

export function keepToken(token){
  return {
    type : types.KEEP_TOKEN,
    value: token
  }
}

export function defaultState(){
  return state;
}