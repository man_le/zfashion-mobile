import * as types from './../types';
import * as componentType from './../../constant/componentType';

const state = {

}

export function buildError(statusCode){
  return {
    type : types.ERROR,
    statusCode: statusCode
  }
}

export function resetError(){
  return {
    type : types.RESET_ERROR,
    statusCode: null
  }
}


export function defaultState(){
  return state;
}