import * as types from './../types';
import * as componentType from './../../constant/componentType';

const state = {
  type: null,
  componentType:componentType.PRODUCT_LIST
}

export function switchTo(componentType){
  return {
    type:types.PRODUCT_LIST,
    componentType: componentType
  }
}

export function defaultState(){
  return state;
}