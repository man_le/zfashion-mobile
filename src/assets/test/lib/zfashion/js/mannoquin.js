



$(function(){	

	$('#index-button-cancel').html(lang['index-button-cancel']);
	$('#index-button-ok').html(lang['index-button-ok']);
	$('#index-modal-title').html(lang['index-modal-title']);

	var item = null;
	$('.z_item').click(function(){
		item = $(this).attr('id');
		$('#index-div-modalContent').html(lang[item]);
	});
	$('#index-button-ok').click(function(){
		$('#'+item).css('display','none');
		$('#confirmModal').modal('hide');
	});
	
	// Do get request to get url : bag, mannequin
	$('#index-img-mannequin').attr('src', maUrl);
	$('#index-img-bag').attr('src', bagUrl);

});