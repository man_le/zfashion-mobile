import { combineEpics } from 'redux-observable';
import {signInEpic} from './access/signInEpics';
import {errorEpic} from './common/errorEpics';

const rootEpic = combineEpics(
    signInEpic,
    // errorEpic
);

export default rootEpic;