import * as types from './../../action/types';
import { signInSuccess} from './../../action/access/signInActions'
import { buildError} from './../../action/common/errorActions'
import signInApi from './../../api/access/SignInApi'
import 'rxjs'
import { Observable } from 'rxjs/Observable'

export const errorEpic = action$ =>
    action$.ofType(types.ERROR)
        .mergeMap(action =>
        signInApi.signIn(action.email, action.password)
        .map(res => signInSuccess(res))
        .catch(err=> Observable.of(types.ERROR))
      )

      