import { AsyncStorage } from 'react-native';
import * as types from './../../action/types';
import { signInSuccess} from './../../action/access/signInActions'
import { buildError} from './../../action/common/errorActions'
import signInApi from './../../api/access/SignInApi'
import 'rxjs'
import { Observable } from 'rxjs/Observable'

async function saveStore(key, value){
    await AsyncStorage.setItem(key, value);
}

export const signInEpic = action$ =>
    action$.ofType(types.SIGN_IN)
    .mergeMap(action =>
        signInApi.signIn(action.email, action.password)
        .map(res => {
            console.log('=========TKKKK===========');
            console.log(res.response.accessToken);
            if(res && res.response) saveStore('utk', res.response.accessToken);
            return signInSuccess(res);
        })
        .catch(err=> Observable.of(types.ERROR).map(err=>buildError('WRONG_USER_PASS')))
      )
